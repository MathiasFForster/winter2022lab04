import java.util.Scanner;
public class Shop {

	public static void main(String[] args) {
		
		System.out.println("Hi customer! Welcome to Mathias' Furniture.");
		System.out.println("Here we give you the price of your future sofa.");
		System.out.println("The fabrics available are: Cotton, Linen, Velvet and Chenille. Also, the number of seats is in the range of 1-10");
		System.out.println("   ");
		
		Sofa[] sofa = new Sofa[4];
		
		Scanner sc = new Scanner(System.in);
		
		for(int i = 0; i < sofa.length; i++) {
			
			System.out.println("");
			System.out.println("Sofa number: " + (i + 1) + "  --------------");
			System.out.println("");
			
			System.out.print("The fabric of your sofa is: ");
			String fabric = sc.next();
			
			System.out.print("The number of seats is: ");
			int nSeats = sc.nextInt();
			
			System.out.print("The style of the sofa is: ");
			String style = sc.next();
			
			sofa[i] = new Sofa(fabric,nSeats,style);
		}
		
		System.out.println(" \n---------- Update ----------\n");
		
		System.out.println("Before: Number of seats = " + sofa[3].getNSeats() + ", Style = " + sofa[3].getStyle() + " And Fabric = " + sofa[3].getFabric());
		
		System.out.print("Update number of seats of your last sofa: ");
		int nSeats = sc.nextInt();
		sofa[3].setNSeats(nSeats);
		
		System.out.print("Update the style of your last sofa: ");
		String style = sc.next();
		sofa[3].setStyle(style);
		
		System.out.println("After: Number of seats = " + sofa[3].getNSeats() + ", Style = " + sofa[3].getStyle() + " And Fabric = " + sofa[3].getFabric());
		
		System.out.println(" \n---------- Info & Price ----------\n");
		System.out.println("Last sofa fabric: " + sofa[3].getFabric());
		System.out.println("Number of seats of last sofa: " + sofa[3].getNSeats());
		System.out.println("Style of the last sofa: " + sofa[3].getStyle());
		System.out.println("");
		
		sofa[3].sofaPrice(sofa[3].getFabric(),sofa[3].getNSeats());
	}
 }
